# dcwebui

![](https://img.shields.io/badge/written%20in-C%2B%2B%20and%20Javascript-blue)

A web interface to an NMDC hub.

A C++ proxy server provides an HTTP interface to NMDC, for an AJAX UI.

Tags: nmdc

This project has been superceded by [nmdc-webfrontend](https://code.ivysaur.me/nmdc-webfrontend/).

## See Also

Original thread: http://forums.apexdc.net/topic/4049-dcwebui/


## Download

- [⬇️ dcwebui-r5.rar](dist-archive/dcwebui-r5.rar) *(63.22 KiB)*
